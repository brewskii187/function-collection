<?php
class log{

    public function __construct(){
        $this->dir = str_replace("includes/log.php", "", __FILE__,$c). "logs/";
        if ($c==0) {
            $this->dir = str_replace("includes\log.php", "", __FILE__, $c). "logs\\";
        }
        unset($c);
        if(!file_exists($this->dir)) {
            mkdir($this->dir, 0755, true);
            $htaccess = fopen($this->dir.'.htaccess', 'a');
            fwrite($htaccess, "Options -Indexes");
            fclose($htaccess);
        }
    }
    public function logError($data){
        $error = fopen($this->dir.'error.log', 'a');
        if(fwrite($error, date('m/j/Y g:i:s A') ."|" . $data ."|". $_SERVER['SCRIPT_NAME'] .  "\r\n")){
        	fclose($error);        	
        	return TRUE;
        }

    }
    public function logAction($data){
        $action = fopen($this->dir.'action.log', 'a');
    	if(fwrite($action, date('m/j/Y g:i:s A') ."|" . $data ."|". $_SERVER['SCRIPT_NAME'] .  "\r\n")){
    		fclose($action);
    		return TRUE;
		}	
        
    }
    public function getLog($log){
        return file($this->dir.$log);

    }
}
?>